package com.singh.service;

import com.singh.dto.UserDto;

import java.util.List;

public interface UserService {

    UserDto saveUser(UserDto userDto);
    List<UserDto> findAll();
    UserDto findById(long id);
    String deleteUser(long id);
    UserDto updateUser(UserDto userDto,long id);
}
