package com.singh.service.Impl;

import com.singh.dao.IUserRepository;
import com.singh.dto.UserDto;
import com.singh.entity.User;
import com.singh.exception.ResourceNotFoundException;
import com.singh.mapper.UserMapper;
import com.singh.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private IUserRepository userRepository;
    @Override
    public UserDto saveUser(UserDto userDto) {
        User saveduser = UserMapper.mapToUser(userDto);
        User save = userRepository.save(saveduser);
        UserDto userDto1 = UserMapper.mapToUserDto(save);
        return userDto1;
    }

    @Override
    public List<UserDto> findAll() {
    List<User> users= userRepository.findAll();
    return users.stream().map(user -> UserMapper.mapToUserDto(user)).collect(Collectors.toList());
    }

    @Override
    public UserDto findById(long id) throws ResourceNotFoundException {
     User user=  userRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Id not found"));
     UserDto dto= UserMapper.mapToUserDto(user);
            return dto;
    }

    @Override
    public String deleteUser(long id) throws ResourceNotFoundException {
        User findbyId = userRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Id not fouind"));
        if (findbyId !=null){
            userRepository.deleteById(id);
        }
        return "USER DELETED";
    }

    @Override
    public UserDto updateUser(UserDto userDto,long id) throws ResourceNotFoundException {
        User existingUser=userRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Id not fouind"));
        existingUser.setFirstName(userDto.getFirstName());
        existingUser.setLastName(userDto.getLastName());
        existingUser.setAge(userDto.getAge());
        existingUser.setEmail(userDto.getEmail());
        existingUser.setPhoneNumber(userDto.getPhoneNumber());
        User savedUser = userRepository.save(existingUser);
        return UserMapper.mapToUserDto(savedUser);
    }


}
