package com.singh.controller;

import com.singh.dto.UserDto;
import com.singh.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/user")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping
    ResponseEntity<UserDto> create ( @RequestBody UserDto userDto){
        UserDto userSavedDto=userService.saveUser(userDto);
        return new ResponseEntity<>(userSavedDto, HttpStatus.CREATED);
    }
     @GetMapping
    ResponseEntity<List<UserDto>> findAll(){
        List<UserDto> allUser = userService.findAll();
        return new ResponseEntity<>(allUser,HttpStatus.OK);

    }
    @GetMapping("{id}")
    public ResponseEntity<UserDto> findById(@PathVariable long id) {
        UserDto findUserById = userService.findById(id);
        return new ResponseEntity<>(findUserById, HttpStatus.OK);
    }
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteUser(@PathVariable long id){
        String deleteUser = userService.deleteUser(id);
        return new ResponseEntity<>(deleteUser,HttpStatus.OK);
    }
    @PutMapping("{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable long id, @RequestBody UserDto userDto){
        UserDto updateUser = userService.updateUser(userDto, id);
        return new ResponseEntity<>(updateUser,HttpStatus.OK);
    }




}
