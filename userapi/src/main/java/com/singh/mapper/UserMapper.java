package com.singh.mapper;

import com.singh.dto.UserDto;
import com.singh.entity.User;

public class UserMapper {
    public  static UserDto mapToUserDto(User user){
        UserDto userDto=new UserDto(
          user.getId(),
          user.getFirstName(),
          user.getLastName(),
          user.getAge(),
          user.getEmail(),
          user.getPhoneNumber()
        );
        return userDto;
    }
    public static User mapToUser(UserDto userDto){
        User user=new User(
                userDto.getId(),
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getAge(),
                userDto.getEmail(),
                userDto.getPhoneNumber()
        );
        return user;
    }
}
